import argparse
import logging
import os
import sys

import numpy as np
import torch
import torch.nn as nn
from torch import optim
from tqdm import tqdm

# from eval import eval_net
# from unet import UNet
from PIL import Image
from torch.utils.tensorboard import SummaryWriter
from dataset import BasicDataset
from torch.utils.data import DataLoader, random_split
import matplotlib.pyplot as plt
import torchvision.transforms as transforms
dir_img = '/Users/yiche/Desktop/val_fapiao_seg_dsy/'
dir_mask = '/Users/yiche/Desktop/val_fapiao_seg_dsy_mask/'
# dir_checkpoint = 'checkpoints/'
train_transform = transforms.Compose([
    # transforms.ToPILImage(),
    transforms.Resize((512, 512)),
    # transforms.RandomResizedCrop(512),
    #transforms.RandomAffine(degrees=15, scale=(0.8, 1.5)),
    # transforms.ColorJitter(brightness=0, contrast=0, saturation=0, hue=0),
    #transforms.CenterCrop(224),
    transforms.ToTensor(),
    # transforms.Normalize([0.5], [0.5])
])

epochs=5
batch_size=1
lr=0.001
val_percent=0.1
save_cp=True
img_scale=0.5
dataset = BasicDataset(dir_img, dir_mask, train_transform, img_scale)
n_val = int(len(dataset) * val_percent)
n_train = len(dataset) - n_val
train, val = random_split(dataset, [n_train, n_val])
train_loader = DataLoader(train, batch_size=batch_size, shuffle=True, num_workers=8, pin_memory=True)
val_loader = DataLoader(val, batch_size=batch_size, shuffle=False, num_workers=8, pin_memory=True, drop_last=True)

writer = SummaryWriter(comment=f'LR_{lr}_BS_{batch_size}_SCALE_{img_scale}')
global_step = 0

logging.info(f'''Starting training:
    Epochs:          {epochs}
    Batch size:      {batch_size}
    Learning rate:   {lr}
    Training size:   {n_train}
    Validation size: {n_val}
    Checkpoints:     {save_cp}
    Images scaling:  {img_scale}
''')

epoch_loss = 0
epoch = 0
im= Image.open("/Users/yiche/Desktop/Pytorch-UNet/data/train/0cdf5b5d0ce1_01.jpg")
ma= Image.open("/Users/yiche/Desktop/Pytorch-UNet/data/train_masks/0cdf5b5d0ce1_01_mask.gif")
# plt.imshow(im)
# plt.show()
# plt.imshow(ma)
# plt.show()
with tqdm(total=n_train, desc=f'Epoch {epoch + 1}/{epochs}', unit='img') as pbar:
    for batch in train_loader:
        imgs = batch['image']
        true_masks = batch['mask']

        imgs = imgs[0].numpy().transpose((1, 2, 0))

        # print(imgs.shape)
        # print(true_masks.shape)
        plt.figure()
        plt.subplot(121)
        plt.imshow(imgs)
        plt.subplot(122)
        plt.imshow(true_masks[0][0].numpy())
        plt.show()
        # plt.imshow(true_masks)
        # plt.show()


