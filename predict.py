import argparse
import logging
import os
from unet import UNet
import numpy as np
import torch
import torch.nn.functional as F
from PIL import Image
from torchvision import transforms
import torchvision.transforms as transforms

train_transform = transforms.Compose([
    transforms.ToPILImage(),
    transforms.Resize((512, 512)),
    # transforms.RandomResizedCrop(512),
    # transforms.RandomAffine(degrees=15, scale=(0.8, 1.5)),
    # transforms.ColorJitter(brightness=0, contrast=0, saturation=0, hue=0),
    # transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize([0.5], [0.5])
])


full_img="/Users/yiche/Desktop/train_fapiao_seg_dsy/411_03330229.jpg"
device="cpu"
scale_factor=1
out_threshold=0.5
img = Image.open(full_img)
print(img)
#img = torch.from_numpy(BasicDataset.preprocess(full_img, scale_factor))
img = train_transform(img)
print(img)
img = img.unsqueeze(0)
img = img.to(device=device, dtype=torch.float32)
net = UNet(n_channels=3, n_classes=1)
with torch.no_grad():
    output = net(img)

    if net.n_classes > 1:
        probs = F.softmax(output, dim=1)
    else:
        probs = torch.sigmoid(output)

    probs = probs.squeeze(0)

    # tf = transforms.Compose(
    #     [
    #         transforms.ToPILImage(),
    #         transforms.Resize(full_img.size[1]),
    #         transforms.ToTensor()
    #     ]
    # )
    # probs = train_transform(probs.cpu())

    full_mask = probs.squeeze().cpu().numpy()




def get_args():
    parser = argparse.ArgumentParser(description='Predict masks from input images',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--model', '-m', default='unet_carvana_scale1_epoch5.pth',
                        metavar='FILE',
                        help="Specify the file in which the model is stored")
    parser.add_argument('--input', '-i', metavar='INPUT', nargs='+',
                        help='filenames of input images', required=True)

    parser.add_argument('--output', '-o', metavar='INPUT', nargs='+',
                        help='Filenames of ouput images')
    parser.add_argument('--viz', '-v', action='store_true',
                        help="Visualize the images as they are processed",
                        default=False)
    parser.add_argument('--no-save', '-n', action='store_true',
                        help="Do not save the output masks",
                        default=False)
    parser.add_argument('--mask-threshold', '-t', type=float,
                        help="Minimum probability value to consider a mask pixel white",
                        default=0.5)
    parser.add_argument('--scale', '-s', type=float,
                        help="Scale factor for the input images",
                        default=0.5)

    return parser.parse_args()


def get_output_filenames(args):
    in_files = args.input
    out_files = []

    if not args.output:
        for f in in_files:
            pathsplit = os.path.splitext(f)
            out_files.append("{}_OUT{}".format(pathsplit[0], pathsplit[1]))
    elif len(in_files) != len(args.output):
        logging.error("Input files and output files are not of the same length")
        raise SystemExit()
    else:
        out_files = args.output

    return out_files


def mask_to_image(mask):
    return Image.fromarray((mask * 255).astype(np.uint8))